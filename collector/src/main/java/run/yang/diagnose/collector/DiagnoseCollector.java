package run.yang.diagnose.collector;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.PrintStream;

public interface DiagnoseCollector {
    CharSequence name();

    void collect(@NonNull Context context, @NonNull PrintStream printer);
}
