package run.yang.diagnose.collector.util.basic;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Created by Yang Tianmei on 2015-09-22.
 */
public class UnitTool {

    /**
     * dp 转为 pixel，返回值为 float 类型
     */
    public static float dp2px_f(@NonNull Context context, float dpValue) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, displayMetrics);
    }

    /**
     * dp 转为 pixel，返回值四舍五入为 int 类型
     */
    public static int dp2px_i(@NonNull Context context, float dpValue) {
        return Math.round(dp2px_f(context, dpValue));
    }
}
