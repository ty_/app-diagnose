package run.yang.diagnose.collector.util.system;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import run.yang.diagnose.collector.util.basic.NumberTool;


/**
 * Created by Yang Tianmei on 2015-09-23.
 */
public class DeviceTool {
    private static final String TAG = DeviceTool.class.getSimpleName();

    @Nullable
    public static String getDeviceId(@NonNull Context context) {
        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getDeviceId();
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            return null;
        }
    }

    public static String getAndroidId(@NonNull Context context) {
        final ContentResolver resolver = context.getContentResolver();
        return Settings.System.getString(resolver, Settings.Secure.ANDROID_ID);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public static long getRamSizeApi16(@NonNull Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        am.getMemoryInfo(memoryInfo);
        return memoryInfo.totalMem;
    }

    /**
     * return value in kB, read from /proc/meminfo
     */
    public static long getRamSizeFromMemInfoInKb() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("/proc/meminfo"))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (TextUtils.isEmpty(line)) {
                    continue;
                }

                if (line.contains("MemTotal")) {
                    final Pattern pattern = Pattern.compile("(\\d+)");
                    final Matcher matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        String ramSize = matcher.group(1);
                        return NumberTool.parseLong(ramSize);
                    }
                    return 0;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * <a href="https://stackoverflow.com/questions/32452924/how-to-detect-android-device-is-64-bit-or-32-bit-processor">
     * How to detect android device is 64 bit or 32 bit processor?</a>
     */
    @SuppressLint("NewApi")
    public static boolean is64BitVm() {
        return Version.atLeast(Version.V21_50) && Build.SUPPORTED_64_BIT_ABIS.length > 0;
    }
}
