package run.yang.diagnose.collector.system;

/**
 * Created by Yang Tianmei on 2015-09-23.
 */
public class TitleContentPair {
    public CharSequence title;
    public CharSequence content;

    public static TitleContentPair create(CharSequence title, CharSequence content) {
        return new TitleContentPair(title, content);
    }

    public TitleContentPair(){
    }

    public TitleContentPair(CharSequence title, CharSequence content){
        this.title = title;
        this.content = content;
    }

    @Override
    public String toString() {
        return "TitleContentPair{" +
                "title=" + title +
                ", content=" + content +
                '}';
    }
}
