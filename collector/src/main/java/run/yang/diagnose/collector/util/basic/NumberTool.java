package run.yang.diagnose.collector.util.basic;

import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * <p>
 * Created by zisxks on 2015-08-13 00:41.
 * </p><p>
 * parse String to corresponding number, catch NumberFormatException.
 * </p><p>
 * if the String to parse is null or empty, return 0 or {@code fallback}
 * </p><p>
 * if catch {@link NumberFormatException}, return 0 or fallback.
 * <p/>
 */
public class NumberTool {

    /**
     * <p>
     * parse String {@code number} to float content.
     * </p>
     * This method do not throw {@link NumberFormatException}
     *
     * @return {@code number} as float content, if {@code number} is null or empty,
     * return 0
     */
    public static float parseFloat(@Nullable String number) {
        return parseFloat(number, 0);
    }

    /**
     * <p>
     * parse String {@code number} to float content.
     * </p>
     * This method do not throw {@link NumberFormatException}
     *
     * @return {@code number} as float content, if {@code number} is null or empty,
     * return {@code fallback}
     */
    public static float parseFloat(@Nullable String number, float fallback) {
        if (TextUtils.isEmpty(number)) {
            return fallback;
        }

        try {
            return Float.parseFloat(number);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return fallback;
        }
    }

    public static int parseInt(@Nullable String value) {
        return parseInt(value, 0);
    }

    public static int parseInt(@Nullable String value, int fallback) {
        if (TextUtils.isEmpty(value)) {
            return fallback;
        }

        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return fallback;
        }
    }

    public static long parseLong(@Nullable String value) {
        return parseLong(value, 0L);
    }

    public static long parseLong(@Nullable String value, long fallback) {
        if (TextUtils.isEmpty(value)) {
            return fallback;
        }

        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return fallback;
        }
    }

    /**
     * @see Long#compare(long, long)
     */
    public static int compare(long lhs, long rhs) {
        return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
    }

    /**
     * @see Integer#compare(int, int)
     */
    public static int compare(int lhs, int rhs) {
        return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
    }

    public static int compare(float lhs, float rhs) {
        return Float.compare(lhs, rhs);
    }

    public static int compare(double lhs, double rhs) {
        return Double.compare(lhs, rhs);
    }
}
