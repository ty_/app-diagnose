package run.yang.diagnose.collector.util.system;

import android.os.Build;

/**
 * Created by ty on 9/21/15.
 */
public class Version {
    private static final int SDK_INT = Build.VERSION.SDK_INT;

    public static final int V8_22 = Build.VERSION_CODES.FROYO;
    public static final int V9_23 = Build.VERSION_CODES.GINGERBREAD;
    public static final int V10_233 = Build.VERSION_CODES.GINGERBREAD_MR1;
    public static final int V11_30 = Build.VERSION_CODES.HONEYCOMB;
    public static final int V12_31 = Build.VERSION_CODES.HONEYCOMB_MR1;
    public static final int V13_32 = Build.VERSION_CODES.HONEYCOMB_MR2;
    public static final int V14_40 = Build.VERSION_CODES.ICE_CREAM_SANDWICH;
    public static final int V15_403 = Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1;
    public static final int V16_41 = Build.VERSION_CODES.JELLY_BEAN;
    public static final int V17_42 = Build.VERSION_CODES.JELLY_BEAN_MR1;
    public static final int V18_43 = Build.VERSION_CODES.JELLY_BEAN_MR2;
    public static final int V19_44 = Build.VERSION_CODES.KITKAT;
    public static final int V20_44W = Build.VERSION_CODES.KITKAT_WATCH;
    public static final int V21_50 = Build.VERSION_CODES.LOLLIPOP;
    public static final int V22_51 = Build.VERSION_CODES.LOLLIPOP_MR1;
    public static final int V23_60 = Build.VERSION_CODES.M;
    public static final int V24_70 = Build.VERSION_CODES.N;

    public static boolean atLeast(int requiredVersion) {
        return SDK_INT >= requiredVersion;
    }

    public static boolean greaterThan(int requiredVersion) {
        return SDK_INT > requiredVersion;
    }

    public static boolean atMost(int requiredVersion) {
        return SDK_INT <= requiredVersion;
    }

    public static boolean lessThan(int requiredVersion) {
        return SDK_INT < requiredVersion;
    }
}
