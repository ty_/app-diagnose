package run.yang.diagnose.collector.app;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.system.ErrnoException;
import android.system.Os;
import android.system.StructStat;

import com.google.common.io.Files;

import java.io.File;
import java.io.PrintStream;
import java.util.Locale;

import run.yang.diagnose.collector.DiagnoseCollector;
import run.yang.diagnose.collector.util.basic.TimeTool;


public class AntNestFilesCollector implements DiagnoseCollector {

    private static final String TAG = "AntNestFilesCollector";

    @Override
    public CharSequence name() {
        return "Ant Nest Files";
    }

    @Override
    public void collect(@NonNull Context context, @NonNull PrintStream printer) {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (externalStorageDirectory == null) {
            printer.println("Environment.getExternalStorageDirectory( return null");
            return;
        }
        // /sdcard/lianjia
        final File lianjiaRoot = new File(externalStorageDirectory, "lianjia");
        listDir(printer, lianjiaRoot);
        if (!lianjiaRoot.exists()) {
            return;
        }

        // /sdcard/lianjia/<package name>
        File[] packageNameFiles = lianjiaRoot.listFiles((dir, name) -> !"com.lianjia.antnest".equals(name));
        if (packageNameFiles != null) {
            for (File packageNameFile : packageNameFiles) {
                listDir(printer, packageNameFile);
                // /sdcard/lianjia/<package name>/log/
                File logInApp = new File(packageNameFile, "log");
                if (logInApp.exists()) {
                    File[] procNameDirs = logInApp.listFiles();
                    if (procNameDirs != null) {
                        for (File procNameFile : procNameDirs) {
                            listDir(printer, procNameFile);
                        }
                    }
                }
            }
        }
        printer.append('\n');

        printer.println("Ant Nest Directory Listing:");
        printer.println();

        File antNestDir = new File(lianjiaRoot, "com.lianjia.antnest");
        for (File file : Files.fileTraverser().depthFirstPreOrder(antNestDir)) {
            if (file.isDirectory()) {
                listDir(printer, file);
            }
        }
    }

    private static void listDir(@NonNull PrintStream printer, @NonNull File dir) {
        printer.append("# ").append("Contents of ").append(dir.getAbsolutePath()).append('\n');
        File[] files = dir.listFiles();
        if (files == null) {
            listSingleFileInfo(printer, dir);
        } else {
            for (File file : files) {
                listSingleFileInfo(printer, file);
            }
            printer.println();
        }
    }

    private static void listSingleFileInfo(@NonNull PrintStream printer, @NonNull File file) {
        if (!file.exists()) {
            printer.println(file.getName() + " not exist");
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                try {
                    final StructStat stat = Os.stat(file.getAbsolutePath());
                    printer.append(Integer.toOctalString(stat.st_mode)).append(' ');
                    printer.append(String.format(Locale.US, "%,10d", stat.st_size)).append(' ');
                    printer.append(TimeTool.formatFormalTime(stat.st_atime * 1000));
                    printer.append(' ');
                    printer.append(TimeTool.formatFormalTime(stat.st_mtime * 1000));
                    printer.append(' ').append(file.getName());
                    printer.append('\n');
                } catch (ErrnoException e) {
                    printer.println(file.getAbsolutePath() + e.getMessage());
                }
            } else {
                collectFilePermissionAttribute(printer, file);
                printer.append(' ');
                printer.printf(Locale.US, "%,10d", file.length()).append(' ');
                printer.append(TimeTool.formatFormalTime(file.lastModified()));
                printer.append(' ').append(file.getName());
                printer.append('\n');
            }
        }
    }

    private static void collectFilePermissionAttribute(@NonNull PrintStream printer, @NonNull File file) {
        printer.append(file.isDirectory() ? 'd' : '-');
        printer.append(file.canRead() ? 'r' : '-');
        printer.append(file.canWrite() ? 'w' : '-');
        printer.append(file.canExecute() ? 'x' : '-');
    }
}
