package run.yang.diagnose.collector.app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;

import java.io.PrintStream;
import java.util.Locale;

import run.yang.diagnose.collector.DiagnoseCollector;
import run.yang.diagnose.collector.util.basic.TimeTool;


public class AppDetailInfoCollector implements DiagnoseCollector {
    private final String mPackageName;

    public AppDetailInfoCollector(@NonNull String packageName) {
        mPackageName = packageName;
    }

    @Override
    public CharSequence name() {
        return "App Detail Info (" + mPackageName + ")";
    }

    @Override
    public void collect(@NonNull Context context, @NonNull PrintStream printer) {
        final PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo(mPackageName, 0);
            printer.format(Locale.US, "package name: %s\n", pi.packageName);
            printer.format(Locale.US, "app name: %s (%s)\n", pi.applicationInfo.loadLabel(pm), pi.applicationInfo.name);
            printer.format(Locale.US, "version: %d(%s)\n", pi.versionCode, pi.versionName);
            printer.format(Locale.US, "lastUpdateTime: %s\n", TimeTool.formatFormalTime(pi.lastUpdateTime));
            printer.format(Locale.US, "firstInstallTime: %s\n", TimeTool.formatFormalTime(pi.firstInstallTime));
        } catch (PackageManager.NameNotFoundException e) {
            printer.format(Locale.US, "package %s not found, %s\n", mPackageName, e);
        }
    }
}
