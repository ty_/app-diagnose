package run.yang.diagnose.collector.util;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import run.yang.diagnose.collector.DiagnoseCollector;
import run.yang.diagnose.collector.app.AntNestFilesCollector;
import run.yang.diagnose.collector.app.AppDetailInfoCollector;
import run.yang.diagnose.collector.app.FileHeaderPrinterCollector;
import run.yang.diagnose.collector.app.InstalledAppCollector;
import run.yang.diagnose.collector.system.BuildConstantsDiagnoseCollector;
import run.yang.diagnose.collector.system.SystemInfoDiagnoseCollector;

public class DiagnoseCollectors {

    public static List<DiagnoseCollector> getAvailableCollectors(@NonNull Context context) {
        final List<DiagnoseCollector> list = new ArrayList<>();

        list.add(new FileHeaderPrinterCollector());
        list.add(new SystemInfoDiagnoseCollector());
        list.add(new BuildConstantsDiagnoseCollector());
        list.add(new AppDetailInfoCollector("com.lianjia.antnest"));
        list.add(new InstalledAppCollector());
        list.add(new AntNestFilesCollector());

        return list;
    }
}
