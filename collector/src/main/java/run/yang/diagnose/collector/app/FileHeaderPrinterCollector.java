package run.yang.diagnose.collector.app;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;

import java.io.PrintStream;
import java.util.Locale;

import run.yang.diagnose.collector.DiagnoseCollector;
import run.yang.diagnose.collector.util.basic.TimeTool;


public class FileHeaderPrinterCollector implements DiagnoseCollector {
    @Override
    public CharSequence name() {
        return "File Header";
    }

    @Override
    public void collect(@NonNull Context context, @NonNull PrintStream printer) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            printer.printf(Locale.US, "App Version: %s(%d)\n", pi.versionName, pi.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        printer.println(TimeTool.formatFormalTime(System.currentTimeMillis()));
    }
}
