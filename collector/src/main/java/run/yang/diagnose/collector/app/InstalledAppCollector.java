package run.yang.diagnose.collector.app;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;

import java.io.PrintStream;
import java.util.List;
import java.util.Locale;

import run.yang.diagnose.collector.DiagnoseCollector;
import run.yang.diagnose.collector.util.basic.TimeTool;


public class InstalledAppCollector implements DiagnoseCollector {
    @Override
    public CharSequence name() {
        return "Installed Apps";
    }

    @Override
    public void collect(@NonNull Context context, @NonNull PrintStream printer) {
        final PackageManager pm = context.getPackageManager();
        final List<ApplicationInfo> installedApplications = pm.getInstalledApplications(0);
        for (ApplicationInfo app : installedApplications) {
            try {
                PackageInfo pi = pm.getPackageInfo(app.packageName, 0);
                printer.format(Locale.US, " %-20s | %-32s | %-18s | %s | %s\n",
                        app.loadLabel(pm), app.packageName, pi.versionName + "(" + pi.versionCode + ")",
                        TimeTool.formatFormalTime(pi.firstInstallTime),
                        TimeTool.formatFormalTime(pi.lastUpdateTime));
            } catch (PackageManager.NameNotFoundException e) {
                printer.println("package " + app.packageName + " not found, " + e.getMessage());
            }
        }
    }
}
