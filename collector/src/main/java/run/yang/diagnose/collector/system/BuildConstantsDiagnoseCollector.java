package run.yang.diagnose.collector.system;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.PrintStream;
import java.util.List;
import java.util.Locale;

import run.yang.diagnose.collector.DiagnoseCollector;


public class BuildConstantsDiagnoseCollector implements DiagnoseCollector {

    @Override
    public CharSequence name() {
        return "Build Constant";
    }

    @Override
    public void collect(@NonNull Context context, @NonNull PrintStream printer) {
        List<TitleContentPair> list = BuildConstantHelper.getByReflection();
        for (TitleContentPair pair : list) {
            printer.format(Locale.US, "%-24s: %s\n", pair.title, pair.content);
        }
    }
}
