package run.yang.diagnose.collector.system;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.support.annotation.NonNull;
import android.text.format.Formatter;

import java.io.File;
import java.io.PrintStream;
import java.util.Locale;

import run.yang.diagnose.collector.DiagnoseCollector;
import run.yang.diagnose.collector.util.system.DeviceTool;


public class SystemInfoDiagnoseCollector implements DiagnoseCollector {
    @Override
    public CharSequence name() {
        return "System Info";
    }

    @Override
    public void collect(@NonNull Context context, @NonNull PrintStream printer) {
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (externalStorageDirectory == null) {
            printer.println("Environment.getExternalStorageDirectory() return null");
        } else {
            StatFs fs = new StatFs(externalStorageDirectory.getAbsolutePath());

            long totalBytes;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                totalBytes = fs.getTotalBytes();
            } else {
                totalBytes = fs.getBlockCount() * fs.getBlockSize();
            }
            printer.format(Locale.US, "Total Bytes: %,3d KB\n", totalBytes / 1024);

            long availableBytes;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                availableBytes = fs.getAvailableBytes();
            } else {
                availableBytes = fs.getAvailableBlocks() * fs.getBlockSize();
            }
            printer.format(Locale.US, "Available Bytes: %,3d KB\n", availableBytes / 1024);
        }

        long ramSizeFromMemInfo = DeviceTool.getRamSizeFromMemInfoInKb();
        String formattedRamSize = Formatter.formatFileSize(context, ramSizeFromMemInfo * (1 << 10));
        printer.format(Locale.US, "RAM Size(/proc/meminfo): %s (%,3d KB)\n", formattedRamSize, ramSizeFromMemInfo);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            final long ramSize = DeviceTool.getRamSizeApi16(context);
            formattedRamSize = Formatter.formatFileSize(context, ramSize);
            printer.format(Locale.US, "RAM Size(MemoryInfo): %s (%,3d KB)\n", formattedRamSize, ramSize / (1 << 10));
        }
    }
}
