package run.yang.diagnose.collector.system;

import android.os.Build;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.RegEx;

import run.yang.diagnose.collector.util.basic.NumberTool;
import run.yang.diagnose.collector.util.basic.TextTool;
import run.yang.diagnose.collector.util.basic.TimeTool;

/**
 * Created by Yang Tianmei on 2015-09-21.
 */
final class BuildConstantHelper {

    private static final String TAG = BuildConstantHelper.class.getSimpleName();

    @NonNull
    static List<TitleContentPair> getByReflection() {
        final List<TitleContentPair> list = new ArrayList<>();

        @RegEx final Pattern expectedFieldNamePattern = Pattern.compile("^[A-Z_][A-Z_0-9]+$");

        Class<?> buildClass = Build.class;
        final String buildPrefix = "";
        collectConstantField(list, expectedFieldNamePattern, buildClass, buildPrefix);

        prettifyBuildTime(list);

        return list;
    }

    private static void prettifyBuildTime(@NonNull List<TitleContentPair> list) {
        for (TitleContentPair bean : list) {
            if ("TIME".contentEquals(bean.title)) {
                final long fallback = -1;
                final long longValue = NumberTool.parseLong(String.valueOf(bean.content), fallback);
                if (longValue != fallback) {
                    bean.content = String.valueOf(bean.content) + '(' + TimeTool.formatFormalTime(longValue) + ')';
                }
                break;
            }
        }
    }

    private static void collectConstantField(@NonNull List<TitleContentPair> list,
                                             @NonNull Pattern pattern,
                                             @NonNull Class<?> clazz,
                                             @NonNull String keyPrefix) {
        final Field[] fields = clazz.getFields();

        for (Field field : fields) {
            if (isExpectedConstantField(field, pattern)) {
                final TitleContentPair bean = new TitleContentPair();
                final boolean deprecated = field.getAnnotation(Deprecated.class) != null;
                if (deprecated) {
                    for (Annotation annotation : field.getAnnotations()) {
                        Log.d(TAG, annotation.toString());
                    }
                }
                bean.title = keyPrefix + (deprecated ? field.getName() + "(Deprecated)" : field.getName());
                Object value = null;
                try {
                    value = field.get(null);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                bean.content = TextTool.toString(value);
                list.add(bean);
            }
        }

        for (Class<?> aClass : clazz.getClasses()) {
            if (Modifier.isStatic(aClass.getModifiers())) {
                String prefix = (TextUtils.isEmpty(keyPrefix) ? aClass.getSimpleName() : keyPrefix + "." + aClass.getSimpleName()) + ".";
                collectConstantField(list, pattern, aClass, prefix);
            }
        }
    }

    private static boolean isExpectedConstantField(@NonNull Field field, @NonNull Pattern pattern) {
        final int modifiers = Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL;
        final String fieldName = field.getName();
        return (field.getModifiers() & modifiers) == modifiers && pattern.matcher(fieldName).matches();
    }
}
