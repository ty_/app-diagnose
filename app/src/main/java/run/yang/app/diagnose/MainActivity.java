package run.yang.app.diagnose;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.base.Throwables;
import com.google.common.io.Files;
import com.google.common.io.LineProcessor;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import run.yang.diagnose.collector.DiagnoseCollector;
import run.yang.diagnose.collector.util.DiagnoseCollectors;

public class MainActivity extends AppCompatActivity {

    private static final String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE};

    private static final int REQ_PERMISSION = RESULT_FIRST_USER + 1;

    private static final String FILE_NAME = "app-diagnose-调试信息.txt";

    private TextView mStatusTextView;
    private TextView mContentTextView;

    private CollectorHelper mCollectorHelper = new CollectorHelper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStatusTextView = findViewById(R.id.status_tv);
        mContentTextView = findViewById(R.id.collected_text_info);

        checkForPermissionAndStart();
    }

    private void checkForPermissionAndStart() {
        final List<String> notGrantedPermission = new ArrayList<>(0);
        for (String permission : PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                notGrantedPermission.add(permission);
            }
        }

        if (notGrantedPermission.isEmpty()) {
            mCollectorHelper.initCollector();
        } else {
            final String[] permissions = notGrantedPermission.toArray(new String[notGrantedPermission.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQ_PERMISSION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissions.length == 0 || grantResults.length == 0) {
            Toast.makeText(this, "未授予需要的权限", Toast.LENGTH_SHORT).show();
        } else {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "未允许全部权限", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            checkForPermissionAndStart();
        }
    }

    @Override
    protected void onDestroy() {
        mCollectorHelper.destroy();
        super.onDestroy();
    }

    private static File getOutputFile() {
        File file = Environment.getExternalStorageDirectory();
        if (file != null) {
            file = new File(file, FILE_NAME);
            return file;
        } else {
            return null;
        }
    }

    private class CollectorHelper {

        private Disposable mGetDiagnoseInfoDisposable;

        private Single<File> getFileSingle() {
            return Single.fromObservable(new Observable<File>() {
                @Override
                protected void subscribeActual(Observer<? super File> observer) {
                    File outputFile = getOutputFile();
                    if (outputFile == null) {
                        observer.onError(new IOException("fail to get diagnose info save directory"));
                        return;
                    }
                    try {
                        try (PrintStream printStream = new PrintStream(outputFile)) {
                            final List<DiagnoseCollector> list = DiagnoseCollectors.getAvailableCollectors(MainActivity.this);
                            for (DiagnoseCollector collector : list) {
                                try {
                                    printStream.println(":========= " + collector.name() + " =========");
                                    collector.collect(MainActivity.this, printStream);
                                } catch (Throwable throwable) {
                                    printStream.println("Exception in " + collector.getClass().getName());
                                    printStream.println(Throwables.getStackTraceAsString(throwable));
                                }
                                printStream.println();
                            }
                        }
                        observer.onNext(outputFile);
                        observer.onComplete();
                    } catch (Throwable e) {
                        observer.onError(e);
                    }
                }
            });
        }

        private void initCollector() {
            ProgressDialog dialog = new ProgressDialog(MainActivity.this);
            dialog.setMessage("正在收集信息");
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.show();

            mGetDiagnoseInfoDisposable = getFileSingle()
                    .map(file -> {
                        String text = Files.asCharSource(file, Charset.defaultCharset()).readLines(new SizeLimitedLineProcessor(file));
                        return Pair.create(file, text);
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(pair -> {
                        dialog.dismiss();
                        mStatusTextView.setText("收集完成, 文件路径：\n/SD卡/" + FILE_NAME);
                        mContentTextView.setText(pair.second);
                    }, throwable -> {
                        dialog.dismiss();
                        mStatusTextView.setText("发生错误");
                        mContentTextView.setText(Throwables.getStackTraceAsString(throwable));
                    });
        }

        void destroy() {
            if (mGetDiagnoseInfoDisposable != null) {
                mGetDiagnoseInfoDisposable.dispose();
                mGetDiagnoseInfoDisposable = null;
            }
        }
    }

    private static class SizeLimitedLineProcessor implements LineProcessor<String> {
        static final int MAX_READ_CHAR_COUNT = 16 * 1024;
        final StringBuilder mBuilder;
        private final File mFile;

        SizeLimitedLineProcessor(@NonNull File file) {
            mFile = file;
            mBuilder = new StringBuilder(Math.min(MAX_READ_CHAR_COUNT, (int) mFile.length()));
        }

        @Override
        public boolean processLine(String line) throws IOException {
            if (mBuilder.length() + line.length() > MAX_READ_CHAR_COUNT + 20) {
                mBuilder.append("....(has more)");
                return false;
            }
            mBuilder.append(line);
            mBuilder.append('\n');
            return true;
        }

        @Override
        public String getResult() {
            return mBuilder.toString();
        }
    }
}
